'''
MIT License

Copyright (c) 2019 Jesus Mastache Caballero

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 '''

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QSpinBox, QFileDialog, QCheckBox, QMessageBox, QLabel
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
from MyAppUI import Ui_MyWindow  # importing our generated file

import sys
import os.path
import subprocess
import fnmatch
import re
import platform

class PackageThread(QThread):
    ChangeApkBuilding = pyqtSignal(int, int)
    UpdateMessageApkBuilding = pyqtSignal(str)
    UpdateLogApkBuilding = pyqtSignal(str)

    MessageLogSize = 92
    
    def __init__(self, ParentObject, CompressFlags, ArchitectureFlags, EngineDir, ProjectDir, ProjectFile, GameVersion, PlatformNames):
        QThread.__init__(self)
        self.ParentObject = ParentObject
        self.CompressFlags = CompressFlags
        self.ArchitectureFlags = ArchitectureFlags
        self.EngineDir = EngineDir
        self.ProjectDir = ProjectDir
        self.ProjectFile = ProjectFile
        self.GameVersion = GameVersion
        self.PlatformNames = PlatformNames


    def __del__(self):
        self.wait()


    def run(self):
        print(platform.system())
        if platform.system() == "Linux":
            self.Extension = "sh"
        elif platform.system() == "Windows":
            self.Extension = "bat"
        elif platform.system() == "Darwin":
            self.Extension = "command"
                
        indexCompress = 1
        for CompressionBuild in self.CompressFlags:
            if (CompressionBuild == False):
                indexCompress += 1
                continue
            indexArch = 1
            for ArchitectureBuild in self.ArchitectureFlags:
                if (ArchitectureBuild == False):
                    indexArch += 1
                    continue
                
                if(indexCompress < 5):
                    openglVersion = 2 
                else:
                    openglVersion = 3
                versionAppCode = f'{indexArch}{openglVersion}{indexCompress}{self.GameVersion}'
                print(versionAppCode)

                # Change StoreVersion
                # Change bBuildForArmV7, bBuildForArm64
                # Change bBuildForES2, bBuildForES31

                with open(self.ProjectDir + "/Config/DefaultEngine.ini", 'r') as file:
                    # read a list of lines into data
                    datafile = file.readlines()
                    indexLine = 0
                    for Line in datafile:
                        if ("StoreVersion" in Line):
                            datafile[indexLine] = "StoreVersion=" + versionAppCode + "\n"
                            print(datafile[indexLine])
                        if ("bBuildForArmV7" in Line):
                            datafile[indexLine] = "bBuildForArmV7=" + str(indexArch == 1) + "\n"
                            print(datafile[indexLine])
                        if ("bBuildForArm64" in Line):
                            datafile[indexLine] = "bBuildForArm64=" + str(indexArch == 2) + "\n"
                            print(datafile[indexLine])
                        if ("bBuildForES2" in Line):
                            datafile[indexLine] = "bBuildForES2=" + str(openglVersion == 2) + "\n"
                            print(datafile[indexLine])
                        if ("bBuildForES31" in Line):
                            datafile[indexLine] = "bBuildForES31=" + str(openglVersion == 3) + "\n"
                            print(datafile[indexLine])
                        indexLine += 1

                # and write everything back
                with open(self.ProjectDir + "/Config/DefaultEngine.ini", 'w') as file:
                    file.writelines( datafile )

                print("------------------------------------------------")

                self.ChangeApkBuilding.emit(indexArch-1, indexCompress-1)
                #self.ParentObject.UpdateInformation(indexArch-1, indexCompress-1)

                self.UpdateMessageApkBuilding.emit("START APK BUILDING")

                #/Engine/Build/BatchFiles
                process = subprocess.Popen([f"{self.EngineDir}/Engine/Build/BatchFiles/RunUAT.{self.Extension}", 
                    "BuildCookRun",
                    f"-project={self.ProjectDir}/{self.ProjectFile}",
                    "-noP4",
                    "-nocompileeditor",
                    f"-targetplatform=Android",
                    f"-cookflavor={self.PlatformNames[indexCompress-1]}",
                    "-package",
                    "-clientconfig=Shipping",
                    "-SkipCookingEditorContent",
                    "-prereqs",
                    "-nodebuginfo",
                    "-cook",
                    "-distribution",
                    "-iterativecooking",
                    "-compressed",
                    "-compile",
                    "-stage",
                    "-build",
                    "-utf8output",
                    "-pak",
                    "-archive",
                    f"-archivedirectory={self.ProjectDir}/Binaries"],
                    stdout=subprocess.PIPE, bufsize=1
                )

                for line in iter(process.stdout.readline, b''):
                    self.BufferString = str(line)
                    if "**********" in self.BufferString:
                        self.BufferString = re.sub('[*\'b]', '', self.BufferString)
                        self.BufferString = self.BufferString.strip()
                        self.BufferString = self.BufferString[0 : len(self.BufferString) -3 : ]
                        self.UpdateMessageApkBuilding.emit(self.BufferString)
                    else:
                        self.BufferString = re.sub('["\'b]', '', self.BufferString)
                        self.BufferString = self.BufferString.strip()
                        if len(self.BufferString) > self.MessageLogSize:
                            self.BufferString = self.BufferString[0 : self.MessageLogSize-3 : ] + "..."
                        else:    
                            self.BufferString = self.BufferString[0 : len(self.BufferString) -2 : ]
                        self.UpdateLogApkBuilding.emit(self.BufferString)
                process.communicate()

                indexArch += 1
            indexCompress += 1
 





class mywindow(QtWidgets.QMainWindow):
    CompressionFlags = [True, False, False, False, False, False]
    ArchitectureFlags = [True, False]
    projectFile = ""
    PlatformNames = ["ETC1", "ATC", "PVRTC", "DXT", "ETC2", "ASTC"]
    ArchNames = ["ARMv7a", "ARMv8a"]

    MessagesStage = 12

    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MyWindow()
        self.ui.setupUi(self)

        self.ui.DirectoryProjectButton.clicked.connect(self.SelectProjectDir)
        self.ui.FolderEngineButton.clicked.connect(self.SelectEngineDir)
        self.ui.PackageButton.clicked.connect(self.RunBuild)

        self.ui.Major_SpinBox.valueChanged.connect(self.UpdatePreview)
        self.ui.Minor_SpinBox.valueChanged.connect(self.UpdatePreview)
        self.ui.BugFix_SpinBox.valueChanged.connect(self.UpdatePreview)

        self.ui.ARMV8_CheckBox.stateChanged.connect(self.ValidateArchitecture)
        self.ui.ARMv7_CheckBox.stateChanged.connect(self.ValidateArchitecture)

        self.ui.ObbInside_CheckBox.stateChanged.connect(self.MiscChange)
        self.ui.Vulkan_CheckBox.stateChanged.connect(self.MiscChange)

        self.ui.ASTC_CheckBox.stateChanged.connect(self.ValidateCompresion)
        self.ui.ETC2_CheckBox.stateChanged.connect(self.ValidateCompresion)
        self.ui.PVRTC_CheckBox.stateChanged.connect(self.ValidateCompresion)
        self.ui.DXT_CheckBox.stateChanged.connect(self.ValidateCompresion)
        self.ui.ATC_CheckBox.stateChanged.connect(self.ValidateCompresion)
        self.ui.ETC1_CheckBox.stateChanged.connect(self.ValidateCompresion)

        if os.path.isfile(".confApkPackager"):
            print("Exist!")
            configRead = open(".confApkPackager")
            
            #Config File(EngineDir, ProjectDir, Compresions, Architecture, Misc, Version)
            self.ui.EngineDir_LineEdit.setText(configRead.readline().replace('\n',''))
            self.ui.ProjectDir_LineEdit.setText(configRead.readline().replace('\n',''))
            
            strConfig = configRead.readline().replace('\n','').split(',')
            self.ui.ASTC_CheckBox.setChecked(strConfig[5] == '1')
            self.ui.ETC2_CheckBox.setChecked(strConfig[4] == '1')
            self.ui.DXT_CheckBox.setChecked(strConfig[3] == '1')
            self.ui.PVRTC_CheckBox.setChecked(strConfig[2] == '1')
            self.ui.ATC_CheckBox.setChecked(strConfig[1] == '1')
            self.ui.ETC1_CheckBox.setChecked(strConfig[0] == '1')

            strConfig = configRead.readline().replace('\n','').split(',')
            self.ui.ARMV8_CheckBox.setChecked(strConfig[1] == '1')
            self.ui.ARMv7_CheckBox.setChecked(strConfig[0] == '1')
            self.ArchitectureFlags[1] = strConfig[1] == '1'
            self.ArchitectureFlags[0] = strConfig[0] == '1'

            strConfig = configRead.readline().replace('\n','').split(',')
            self.ui.ObbInside_CheckBox.setChecked(strConfig[0] == '1')
            self.ui.Vulkan_CheckBox.setChecked(strConfig[1] == '1')

            strConfig = configRead.readline().replace('\n','').split(',')
            self.ui.Major_SpinBox.setValue(int(strConfig[0]))
            self.ui.Minor_SpinBox.setValue(int(strConfig[1]))
            self.ui.BugFix_SpinBox.setValue(int(strConfig[2]))
            configRead.close()
        else:
            print("Fail!")
            configRead = open(".confApkPackager", "w")

            #Config File(EngineDir, ProjectDir, Compresions, Architecture, Misc, Version)
            strConfig = "/home\n\n1,0,0,0,0,0\n1,0\n1,0\n1,0,0"
            configRead.write(strConfig)
            configRead.close()

            self.ui.EngineDir_LineEdit.setText("/home")


    def MiscChange(self, state):
        self.SaveConfig()


    def ValidateArchitecture(self, state):
        if (self.ui.ARMV8_CheckBox.isChecked() == False and self.ui.ARMv7_CheckBox.isChecked() == False):
            self.ui.ARMv7_CheckBox.setChecked(True)
        self.SaveConfig()


    def ValidateCompresion(self, state):
        if (self.ui.ASTC_CheckBox.isChecked() == False and self.ui.ETC2_CheckBox.isChecked() == False and
        self.ui.PVRTC_CheckBox.isChecked() == False and self.ui.DXT_CheckBox.isChecked() == False and
        self.ui.ATC_CheckBox.isChecked() == False and self.ui.ETC1_CheckBox.isChecked() == False):
            self.ui.ETC1_CheckBox.setChecked(True)
        self.SaveConfig()


    def UpdatePreview(self):
        MajorValue = self.ui.Major_SpinBox.value()
        MinorValue = self.ui.Minor_SpinBox.value()
        BugValue = self.ui.BugFix_SpinBox.value()

        self.ui.Preview_Label.setText(str(MajorValue) + "." + str(MinorValue) + "." + str(BugValue))
        self.SaveConfig()


    def SelectEngineDir(self):
        dir = QFileDialog.getExistingDirectory(None, 'Select a folder:', self.ui.EngineDir_LineEdit.text(), QFileDialog.ShowDirsOnly)
        if os.path.isdir(dir + "/Engine/Build/BatchFiles") == False:
            QMessageBox.warning(self, "Error", "Not an UE4 Engine Directory", QMessageBox.Ok)
            return

        self.ui.EngineDir_LineEdit.setText(dir)
        self.SaveConfig()


    def SelectProjectDir(self):
        dir = QFileDialog.getExistingDirectory(None, 'Select a folder:', self.ui.ProjectDir_LineEdit.text(), QFileDialog.ShowDirsOnly)
        if self.MyFind("*.uproject", dir) == False:
            QMessageBox.warning(self, "Error", "Not an UE4 Project Directory", QMessageBox.Ok)
            return

        self.ui.ProjectDir_LineEdit.setText(dir)
        self.SaveConfig()

        
    def MyFind(self, pattern, path):
        result = []
        for file in os.listdir(path):
            if fnmatch.fnmatch(file, pattern):
                self.projectFile = file
                return True
        return False


    def SaveConfig(self):
        configRead = open(".confApkPackager", "w")
        #Config File(EngineDir, ProjectDir, Compresions, Architecture, Misc, Version)
        strConfig = ""
        strConfig += self.ui.EngineDir_LineEdit.text() + "\n" + self.ui.ProjectDir_LineEdit.text() + "\n"
            
        strConfig += str(int(self.ui.ETC1_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.ATC_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.PVRTC_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.DXT_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.ETC2_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.ASTC_CheckBox.isChecked())) + "\n"

        strConfig += str(int(self.ui.ARMv7_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.ARMV8_CheckBox.isChecked())) + "\n"

        strConfig += str(int(self.ui.ObbInside_CheckBox.isChecked())) + ","
        strConfig += str(int(self.ui.Vulkan_CheckBox.isChecked())) + "\n"

        strConfig += str(self.ui.Major_SpinBox.value()) + ","
        strConfig += str(self.ui.Minor_SpinBox.value()) + ","
        strConfig += str(self.ui.BugFix_SpinBox.value())

        configRead.write(strConfig)
        configRead.close()


    def RunBuild(self):
        if os.path.isdir(self.ui.EngineDir_LineEdit.text() + "/Engine/Build/BatchFiles") == False:
            QMessageBox.critical(self, "Error", "Not a valid UE4 Engine Directory", QMessageBox.Ok)
            return

        if self.ui.ProjectDir_LineEdit.text() == "" or self.MyFind("*.uproject", self.ui.ProjectDir_LineEdit.text()) == False:
            QMessageBox.critical(self, "Error", "Not a valid UE4 Project Directory", QMessageBox.Ok)
            return

        self.CompressionFlags[0] = self.ui.ETC1_CheckBox.isChecked()
        self.CompressionFlags[1] = self.ui.ATC_CheckBox.isChecked()
        self.CompressionFlags[2] = self.ui.PVRTC_CheckBox.isChecked()
        self.CompressionFlags[3] = self.ui.DXT_CheckBox.isChecked()
        self.CompressionFlags[4] = self.ui.ETC2_CheckBox.isChecked()
        self.CompressionFlags[5] = self.ui.ASTC_CheckBox.isChecked()

        self.ArchitectureFlags[0] = self.ui.ARMv7_CheckBox.isChecked()
        self.ArchitectureFlags[1] = self.ui.ARMV8_CheckBox.isChecked()

        # How many apks we're going to make
        self.ApksToBuild = self.CalculateApks()
        if (self.ApksToBuild == 0):
            QMessageBox.critical(self, "Error", "For some reason, 0 apks are going to be build", QMessageBox.Ok)
            return
        
        self.ui.PackageButton.setEnabled(False)
        self.ui.FolderEngineButton.setEnabled(False)
        self.ui.DirectoryProjectButton.setEnabled(False)
        self.ui.ProgressBar.setValue(0)
        self.ui.ProgressBar.setMaximum(self.ApksToBuild * self.MessagesStage -1)
        self.indexStageBuild = -1
        self.indexApkBuild = 0

        MajorValue = self.ui.Major_SpinBox.value()
        MinorValue = self.ui.Minor_SpinBox.value()
        BugValue = self.ui.BugFix_SpinBox.value()

        #versionAppCode = f'{MajorValue}.{MinorValue:02}.{BugValue:02}'
        #print(versionAppCode)

        # Change bPackageDataInsideApk
        # Change bSupportsVulkan
        # Change VersionDisplayName
        with open(self.ui.ProjectDir_LineEdit.text() + "/Config/DefaultEngine.ini", 'r') as file:
            # read a list of lines into data
            datafile = file.readlines()
            indexLine = 0
            for Line in datafile:
                if ("VersionDisplayName" in Line):
                    datafile[indexLine] = "VersionDisplayName=" + f"{MajorValue}.{MinorValue}.{BugValue}\n"
                    print(datafile[indexLine])
                if ("bSupportsVulkan" in Line):
                    datafile[indexLine] = "bSupportsVulkan=" + str(self.ui.Vulkan_CheckBox.isChecked()) + "\n"
                    print(datafile[indexLine])
                if ("bPackageDataInsideApk" in Line):
                    datafile[indexLine] = "bPackageDataInsideApk=" + str(self.ui.ObbInside_CheckBox.isChecked()) + "\n"
                    print(datafile[indexLine])
                indexLine += 1

        # and write everything back
        with open(self.ui.ProjectDir_LineEdit.text() + "/Config/DefaultEngine.ini", 'w') as file:
            file.writelines( datafile )

        print("***************************************")

        self.packageThread = PackageThread(self,
            self.CompressionFlags,
            self.ArchitectureFlags, 
            self.ui.EngineDir_LineEdit.text(), 
            self.ui.ProjectDir_LineEdit.text(), 
            self.projectFile, 
            f"{MajorValue}{MinorValue:02}{BugValue:02}",
            self.PlatformNames)
        self.packageThread.ChangeApkBuilding.connect(self.UpdateInformation)
        self.packageThread.UpdateMessageApkBuilding.connect(self.UpdateApkStageBuild)
        self.packageThread.UpdateLogApkBuilding.connect(self.UpdateApkLogBuild)
        self.packageThread.finished.connect(self.Done)
        self.packageThread.start()


    def CalculateApks(self):
        countApk = 0
        for CompressionBuild in self.CompressionFlags:
            if (CompressionBuild == False):
                continue
            for ArchitectureBuild in self.ArchitectureFlags:
                if (ArchitectureBuild == False):
                    continue
                countApk += 1
        return countApk


    @pyqtSlot(str)
    def UpdateApkLogBuild(self, BuildLog):
        self.ui.LogLabel.setText(BuildLog)


    @pyqtSlot(str)
    def UpdateApkStageBuild(self, BuildStage):
        self.ui.InfoProgressLabel.setText(BuildStage)
        self.ui.ProgressBar.setValue(self.indexStageBuild)
        self.indexStageBuild += 1


    @pyqtSlot(int, int)
    def UpdateInformation(self, IndexArch, IndexFlavor):
        self.ui.InformationLabel.setText(f"Building {self.ArchNames[IndexArch]} {self.PlatformNames[IndexFlavor]} ({self.indexApkBuild+1}/{self.ApksToBuild})")
        self.ui.ProgressBar.setValue(self.indexApkBuild)
        self.indexApkBuild += 1
        self.indexStageBuild += 1


    def Done(self):
        self.ui.PackageButton.setEnabled(True)
        self.ui.FolderEngineButton.setEnabled(True)
        self.ui.DirectoryProjectButton.setEnabled(True)
        self.ui.InformationLabel.setText("Progress Information")
        self.ui.InfoProgressLabel.setText("Done")
        self.ui.ProgressBar.setValue(self.ApksToBuild * self.MessagesStage -1);
        QMessageBox.information(self, "Done!", "Done making APK", QMessageBox.Ok)


app = QtWidgets.QApplication([])
application = mywindow()
application.show()
sys.exit(app.exec())
