# **Apk Packager for UnrealEngine4**

This little app is used for build multiple apk files easier.
The app asumes that you already have installed and configured Android SDK and NDK in your computer and project.

At the moment, to run the app you have to do it from commandline

```
python ApkPackagerUE.py
```

The VersionCode for each APK follows this format

| x | x | x | x | xx | xx |
|---|---|---|---|----|----|
| 1 | 2 | 3 | 4 |  5 |  6 |

1. ABI Identifier
    - 2 -> arm64
    - 1 -> armv7
2. OpenGL Version
    - 3 -> ES3.1
    - 2 -> ES2.0
3. Texture Format Compression
    - 6 -> ASTC
    - 5 -> ETC2
    - 3 -> DXT
    - 3 -> PVRTC
    - 2 -> ATC
    - 1 -> ETC1
4. Mayor version (New Version) (0-9)
5. Minor version (New Content) (0-99)
6. BugFix version (0-99)

The app was only tested with Python-3.6.7 and UnrealEngine4.21
It was made using PyQt5 (5.12).

# NOTE: Python3 and PyQt5 are needed to run the application

Programmed by:
 - Jesús Mastache Caballero (BrunoCooper17)

[Portafolio](https://brunocooper17.gitlab.io/portafolio/projects)
[Twitter](https://twitter.com/BrunoCooper_17)
[LinkedIn](https://www.linkedin.com/in/jesus-mastache-25265952/)
